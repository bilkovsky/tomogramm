﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace bilkovsky_tomogram_visualizer
{
    class Bin
    {
        public static int x, y, z;
        public static float xf, yf, zf;
        public static short[] array;
        public Bin() { }

        public void readBIN( string path)
        {
            if(File.Exists(path))
            {
                BinaryReader reader = new BinaryReader(File.Open(path, FileMode.Open));

                x = reader.ReadInt32();
                y = reader.ReadInt32();
                z = reader.ReadInt32();

                xf = reader.ReadSingle();
                yf = reader.ReadSingle();
                zf = reader.ReadSingle();

                int arraySize = x * y * z;
                array = new short[arraySize];
                for(int i = 0; i < arraySize; ++i)
                {
                    array[i] = reader.ReadInt16();
                }
            }
        }
    }
}
