﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace bilkovsky_tomogram_visualizer
{
    public partial class Form1 : Form
    {
        Bin bin = new Bin();
        View view = new View();
        int x1, y1, x2, y2;
        float V, Val;
        int FrameCount = 0;
        DateTime NextFPSUpdate = DateTime.Now.AddSeconds(1);
        int currentLayer = 0;
        bool loaded = false;
        bool needReload = true;
        int mode = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string str = dialog.FileName;
                bin.readBIN(str);
                glControl1.Width = Bin.x;
                glControl1.Height = Bin.y;
                view.SetupView(glControl1.Width, glControl1.Height);
                loaded = true;
                trackBar1.Maximum = Bin.z - 1;
                trackBar3.Minimum = -1000;
                trackBar3.Maximum = 1000;
                trackBar2.Minimum = 1;
                trackBar2.Maximum = 2000;
                glControl1.Invalidate();
            }
        }

        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            if (loaded)
            {
                switch (mode)
                {
                    case 1:
                        if (needReload)
                        {
                            view.GenerateTextureImage(currentLayer);
                            view.Load2dTexture();
                            needReload = false;
                        }
                        view.DrawTexture();
                        glControl1.SwapBuffers();
                        break;
                    case 2:
                        view.DrawQuads(currentLayer);
                        glControl1.SwapBuffers();
                        break;
                }
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            currentLayer = trackBar1.Value;
            needReload = true;
        }
        private void Application_idle(object sender, EventArgs e)
        {
            while (glControl1.IsIdle)
            {
                DisplayFPS();
                glControl1.Invalidate();
            }
        }
        private void DisplayFPS()
        {
            if (DateTime.Now >= NextFPSUpdate)
            {
                this.Text = String.Format("CT Visualizer ( fps = {0})", FrameCount);
                NextFPSUpdate = DateTime.Now.AddSeconds(1);
                FrameCount = 0;
            }
            FrameCount++;
        }
        private void glControl1_SizeChanged(object sender, EventArgs e)
        {
            if (loaded)
            {
                view.SetupView(glControl1.Width, glControl1.Height);
                glControl1.Invalidate();
                Form1_Load_1(sender, e);
            }
        }

        private void Quads_CheckedChanged(object sender, EventArgs e)
        {
            mode = 2;
        }

        private void Texture_CheckedChanged(object sender, EventArgs e)
        {
            mode = 1;
        }


        private void Form1_Load_1(object sender, EventArgs e)
        {
            Application.Idle += Application_idle;
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            View.width = trackBar2.Value;
            needReload = true;
        }
        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            View.min = trackBar3.Value;
            needReload = true;
        }

        private void glControl1_MouseUp(object sender, MouseEventArgs e)
        {
            Val = 0;
            x2 = MousePosition.X;
            y2 = MousePosition.Y;
            label4.Text = "x = " + x2.ToString() + " y = " + y2.ToString();
            x2 = Math.Abs(x2 - x1);
            y2 = Math.Abs(y2 - y1);
            V = x2 * y2 * Bin.xf * Bin.yf * Bin.zf;
            label1.Text = "Volume = " + V.ToString();
            for(int i = 0; i < x2; ++i)
                for(int j = 0; j < y2; ++j)
                {
                    Val += Bin.array[ i + j * Bin.x + currentLayer * Bin.x * Bin.y];
                }
            Val /= x2 * y2;
            label2.Text = "Value = " + Val.ToString();
        }

        private void glControl1_MouseDown(object sender, MouseEventArgs e)
        {
            x1 = MousePosition.X;
            y1 = MousePosition.Y;
            label3.Text = "x = " + x1.ToString() +  " y = " + y1.ToString();
        }
    }
}
